
interface axi_if
#(
  parameter int unsigned AXI_ADDR_WIDTH = 0,
  parameter int unsigned AXI_DATA_WIDTH = 0,
  parameter int unsigned AXI_ID_WIDTH   = 0,
  parameter int unsigned AXI_USER_WIDTH = 0
);
  
  import axi_pkg::*;

  localparam int unsigned AXI_STRB_WIDTH = AXI_DATA_WIDTH / 8;

  typedef logic [AXI_ID_WIDTH-1:0]   id_t;
  typedef logic [AXI_ADDR_WIDTH-1:0] addr_t;
  typedef logic [AXI_DATA_WIDTH-1:0] data_t;
  typedef logic [AXI_STRB_WIDTH-1:0] strb_t;
  typedef logic [AXI_USER_WIDTH-1:0] user_t;

  id_t              awid;
  addr_t            awaddr;
  axi_pkg::len_t    awlen;
  axi_pkg::size_t   awsize;
  axi_pkg::burst_t  awburst;
  logic             awlock;
  axi_pkg::cache_t  awcache;
  axi_pkg::prot_t   awprot;
  axi_pkg::qos_t    awqos;
  axi_pkg::region_t awregion;
  axi_pkg::atop_t   awatop;
  user_t            awuser;
  logic             awvalid;
  logic             awready;

  data_t            wdata;
  strb_t            wstrb;
  logic             wlast;
  user_t            wuser;
  logic             wvalid;
  logic             wready;

  id_t              bid;
  axi_pkg::resp_t   bresp;
  user_t            buser;
  logic             bvalid;
  logic             bready;

  id_t              arid;
  addr_t            araddr;
  axi_pkg::len_t    arlen;
  axi_pkg::size_t   arsize;
  axi_pkg::burst_t  arburst;
  logic             arlock;
  axi_pkg::cache_t  arcache;
  axi_pkg::prot_t   arprot;
  axi_pkg::qos_t    arqos;
  axi_pkg::region_t arregion;
  user_t            aruser;
  logic             arvalid;
  logic             arready;

  id_t              rid;
  data_t            rdata;
  axi_pkg::resp_t   rresp;
  logic             rlast;
  user_t            ruser;  
  logic             rvalid;
  logic             rready;

  modport master (
    output awid, awaddr, awlen, awsize, awburst, awlock, awcache, awprot, awqos, awregion, awatop, awuser, awvalid, input awready,
    output wdata, wstrb, wlast, wuser, wvalid, input wready,
    input bid, bresp, buser, bvalid, output bready,
    output arid, araddr, arlen, arsize, arburst, arlock, arcache, arprot, arqos, arregion, aruser, arvalid, input arready,
    input rid, rdata, rresp, rlast, ruser, rvalid, output rready
  );

  modport slave (
    input awid, awaddr, awlen, awsize, awburst, awlock, awcache, awprot, awqos, awregion, awatop, awuser, awvalid, output awready,
    input wdata, wstrb, wlast, wuser, wvalid, output wready,
    output bid, bresp, buser, bvalid, input bready,
    input arid, araddr, arlen, arsize, arburst, arlock, arcache, arprot, arqos, arregion, aruser, arvalid, output arready,
    output rid, rdata, rresp, rlast, ruser, rvalid, input rready
  );

  modport monitor (
    input awid, awaddr, awlen, awsize, awburst, awlock, awcache, awprot, awqos, awregion, awatop, awuser, awvalid, awready,
          wdata, wstrb, wlast, wuser, wvalid, wready,
          bid, bresp, buser, bvalid, bready,
          arid, araddr, arlen, arsize, arburst, arlock, arcache, arprot, arqos, arregion, aruser, arvalid, arready,
          rid, rdata, rresp, rlast, ruser, rvalid, rready
  );

endinterface