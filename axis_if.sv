
interface axis_if
#(
  parameter int AXI_DATA_WIDTH = 0
);

  logic                      tvalid;
  logic                      tready;
  logic                      tlast;
  logic [AXI_DATA_WIDTH-1:0] tdata;

  modport master (
    output tvalid, input tready, output tlast, tdata
  );

  modport slave (
    input tvalid, output tready, input tlast, tdata
  );

endinterface
