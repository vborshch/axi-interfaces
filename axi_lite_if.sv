interface axi_lite_if
#(
  parameter int unsigned AXI_ADDR_WIDTH = 0,
  parameter int unsigned AXI_DATA_WIDTH = 0
);

  localparam int unsigned AXI_STRB_WIDTH = AXI_DATA_WIDTH / 8;

  typedef logic [AXI_ADDR_WIDTH-1:0] addr_t;
  typedef logic [AXI_DATA_WIDTH-1:0] data_t;
  typedef logic [AXI_STRB_WIDTH-1:0] strb_t;

  // AW channel
  addr_t          awaddr;
  axi_pkg::prot_t awprot;
  logic           awvalid;
  logic           awready;

  data_t          wdata;
  strb_t          wstrb;
  logic           wvalid;
  logic           wready;

  axi_pkg::resp_t bresp;
  logic           bvalid;
  logic           bready;

  addr_t          araddr;
  axi_pkg::prot_t arprot;
  logic           arvalid;
  logic           arready;

  data_t          rdata;
  axi_pkg::resp_t rresp;
  logic           rvalid;
  logic           rready;

  modport master (
    output awaddr, awprot, awvalid, input awready,
    output wdata, wstrb, wvalid, input wready,
    input bresp, bvalid, output bready,
    output araddr, arprot, arvalid, input arready,
    input rdata, rresp, rvalid, output rready
  );

  modport slave (
    input awaddr, awprot, awvalid, output awready,
    input wdata, wstrb, wvalid, output wready,
    output bresp, bvalid, input bready,
    input araddr, arprot, arvalid, output arready,
    output rdata, rresp, rvalid, input rready
  );

  modport monitor (
    input awaddr, awprot, awvalid, awready,
          wdata, wstrb, wvalid, wready,
          bresp, bvalid, bready,
          araddr, arprot, arvalid, arready,
          rdata, rresp, rvalid, rready
  );

endinterface